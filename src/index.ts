import express from 'express'
const app = express(),
    port = 3000

app.get('/', (req, res): void => {
    res.send('Hello World!')
})

app.listen(port, (): void => {
    console.log(`Listening on port ${port}`)
})